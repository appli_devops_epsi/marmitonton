## Classe B3 - C4

### Etudiants :

    - AZIZIAN Rafayel
    - MULLER Tristan
    - NASRI Sofiane
    - SIMON Jean-François

### Lien vers notre Trello pour la gestion de projet

https://trello.com/b/pS3rBDQt/kanban-marmitonton

### URLs des services déployés sur un VPS

L'application se trouve à l'url : http://54.36.191.217:8888
La base de données MySQL se trouve à l'adresse : http://54.36.191.217:3308

### Connexion à la base de donnée

La connexion se fait via un SGBD, avec les identifiants suivants :

    - Nom de la base de données : marmitonton
    - User : root 
    - Mot de passe : alamodedecheznous

### Container Registry

Dans le registre de conteneur de Gitlab, nous y avons connecté le pipeline pour que la dernière version de l'image y soit ajouté.
Notre image de conteneur du projet est le LATEST.
https://gitlab.com/appli_devops_epsi/marmitonton/container_registry/3122598

### GitLab-CI

Voici le lien vers les pipelines avec leur résultats et les logs correspondant à chaque stage.
https://gitlab.com/appli_devops_epsi/marmitonton/-/pipelines

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

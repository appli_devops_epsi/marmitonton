insert into INGREDIENT_ENTITY (name) values ('Chocolat');
insert into INGREDIENT_ENTITY (name) values ('Banane');
insert into INGREDIENT_ENTITY (name) values ('Asperge');
insert into INGREDIENT_ENTITY (name) values ('Sucre roux');
insert into INGREDIENT_ENTITY (name) values ('Cervelle de crocodile tropical sans écailles');
insert into INGREDIENT_ENTITY (name) values ('Oeil de fourmis');
insert into INGREDIENT_ENTITY (name) values ('Algue verte');

insert into RECIPE_ENTITY (name) values ('Space Cake');
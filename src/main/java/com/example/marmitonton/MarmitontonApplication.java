package com.example.marmitonton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
public class MarmitontonApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarmitontonApplication.class, args);
		Logger.getLogger("ENV VARIABLE => " + System.getenv("DB_SERVER"));
	}

}
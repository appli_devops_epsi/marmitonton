package com.example.marmitonton.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.marmitonton.entities.IngredientEntity;
import com.example.marmitonton.entities.RecipeEntity;
import com.example.marmitonton.services.IngredientService;
import com.example.marmitonton.services.RecipeService;

import java.util.List;
import java.util.logging.Logger;

@Controller
public class RecipeController {

    @Autowired
    private RecipeService recipeService;
    @Autowired
    private IngredientService ingredientService;

    private static final String RECIPE_ATTRIBUTE_NAME = "recipes";
    private static final String STATUS_ATTRIBUTE_NAME = "status";

    @GetMapping("/recipe/create")
    public String createRecipe(
            @RequestParam(name = "name", required = false) String name,
            Model model) {
        if (name != null && ingredientService.createIngredient(new IngredientEntity(name)) != null) {
            model.addAttribute(STATUS_ATTRIBUTE_NAME, "Ingrédient créé avec succés");
        }

        model.addAttribute("recipe", new RecipeEntity());
        model.addAttribute("allIngredients", ingredientService.getAllIngredients());
        return "recipeCreate";
    }

    @PostMapping("/recipe/create")
    public String createRecipe(@ModelAttribute RecipeEntity recipe, Model model) {
        model.addAttribute("recipe", recipe);
        model.addAttribute("allIngredients", ingredientService.getAllIngredients());

        final RecipeEntity r = recipeService.createRecipe(recipe);
        if (r != null) {
            model.addAttribute(STATUS_ATTRIBUTE_NAME, "Recette créée avec succés : " + r.toString());
        } else {
            model.addAttribute(STATUS_ATTRIBUTE_NAME, "ERREUR ! Impossible de créer la recette");
        }

        return "recipeCreate";
    }

    @GetMapping("/recipe/edit")
    public String editRecipe(Model model) {
        model.addAttribute("recipe", new RecipeEntity());
        model.addAttribute("allRecipes", recipeService.getAllRecipes());
        model.addAttribute("allIngredients", ingredientService.getAllIngredients());
        model.addAttribute(STATUS_ATTRIBUTE_NAME, Arrays.toString(recipeService.getAllRecipes().toArray()));
        return "recipeEdit";
    }

    @PostMapping("/recipe/edit")
    public String editRecipe(
            @RequestParam(name = "recipeId", required = false) Integer recipeId,
            @ModelAttribute RecipeEntity recipe,
            Model model) {
        recipe.setId(recipeId);
        model.addAttribute("recipe", recipe);
        model.addAttribute("allRecipes", recipeService.getAllRecipes());
        model.addAttribute("allIngredients", ingredientService.getAllIngredients());

        final RecipeEntity updatedRecipe = recipeService.updateRecipe(recipe);

        if (updatedRecipe != null) {
            model.addAttribute(STATUS_ATTRIBUTE_NAME, "Recette modifiée avec succés : " + updatedRecipe.toString());
        } else {
            model.addAttribute(STATUS_ATTRIBUTE_NAME, "ERREUR ! Impossible de créer la recette");
        }

        return "recipeEdit";
    }

    @GetMapping("/recipe/search")
    public String searchRecipe(
            @RequestParam(name = "name", required = false) String name,
            Model model) {
        if (name != null) {
            List<RecipeEntity> recipes = recipeService.findByNameContains(name);
            model.addAttribute(RECIPE_ATTRIBUTE_NAME, recipes);

            if (recipes.isEmpty()) {
                model.addAttribute(STATUS_ATTRIBUTE_NAME, "Aucun résultat");
            } else {
                model.addAttribute(STATUS_ATTRIBUTE_NAME, recipes.size() + " résultat(s) trouvé(s) :");
            }
        } else {
            model.addAttribute(STATUS_ATTRIBUTE_NAME, "");
        }

        return "recipeSearch";
    }
}

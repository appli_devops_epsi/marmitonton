package com.example.marmitonton.services;

import java.util.*;

import javax.el.MethodNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Service;

import com.example.marmitonton.entities.IngredientEntity;
import com.example.marmitonton.repositories.IngredientRepository;

@Service
public class IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    public IngredientEntity createIngredient(IngredientEntity ingredientEntity) {
        return this.ingredientRepository.save(ingredientEntity);
    }

    public Optional<IngredientEntity> getIngredient(String name) {
        return this.ingredientRepository.findById(name);
    }

    public List<IngredientEntity> getIngredientByNameLike(String name) {
        return this.ingredientRepository.findByNameLike(name);
    }

    public boolean deleteIngredient(String name) {
        try {
            this.ingredientRepository.deleteById(name);
            return true;
        } catch (DataAccessResourceFailureException e) {
            return false;
        }
    }

    public IngredientEntity updateIngredient(IngredientEntity ingredientEntity) {
        if (Objects.isNull(ingredientEntity.getName())) {
            throw new MethodNotFoundException("Aucun ID trouvé, mise à jour impossible");
        }

        return this.ingredientRepository.save(ingredientEntity);
    }

    public List<IngredientEntity> getAllIngredients() {
        return new ArrayList<>(this.ingredientRepository.findAll());
    }
}

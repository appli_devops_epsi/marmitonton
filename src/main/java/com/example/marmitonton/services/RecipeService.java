package com.example.marmitonton.services;

import java.lang.invoke.WrongMethodTypeException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.el.MethodNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Service;

import com.example.marmitonton.entities.RecipeEntity;
import com.example.marmitonton.repositories.RecipeRepository;

@Service
public class RecipeService {
    @Autowired
    private RecipeRepository recipeRepository;

    public RecipeEntity createRecipe(RecipeEntity recipeEntity) {
        if (Objects.nonNull(recipeEntity.getId())) {
            throw new WrongMethodTypeException("Une création d'élément ne devrait pas contenir d'id");
        }

        return this.recipeRepository.save(recipeEntity);
    }

    public Optional<RecipeEntity> getRecipe(Integer id) {
        return this.recipeRepository.findById(id);
    }

    public List<RecipeEntity> findByNameContains(String name) {
        return this.recipeRepository.findByNameContains(name);
    }

    public boolean deleteRecipe(Integer id) {
        try {
            this.recipeRepository.deleteById(id);
            return true;
        } catch (DataAccessResourceFailureException e) {
            return false;
        }
    }

    public RecipeEntity updateRecipe(RecipeEntity recipeEntity) {
        if (Objects.isNull(recipeEntity.getId())) {
            throw new MethodNotFoundException("Aucun ID trouvé, mise à jour impossible");
        }

        return this.recipeRepository.save(recipeEntity);
    }

    public List<RecipeEntity> getAllRecipes() {
        return this.recipeRepository.findAll();
    }
}

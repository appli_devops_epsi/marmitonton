package com.example.marmitonton.entities;

import javax.persistence.*;
import java.util.*;

@Entity
public class RecipeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<IngredientEntity> ingredients;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getIngredientsNames() {
        StringBuilder names = new StringBuilder();

        final Iterator<IngredientEntity> ingredientIterator = ingredients.iterator();

        while (ingredientIterator.hasNext()) {
            names.append(ingredientIterator.next().getName());

            if (ingredientIterator.hasNext()) {
                names.append(", ");
            }
        }

        return names.toString();
    }

    public void setIngredientNames(String[] ingredientNames) {
        for (String ingredientName : ingredientNames) {
            this.ingredients.add(new IngredientEntity(ingredientName));
        }
    }

    public void addIngredients(IngredientEntity... ingredients) {
        for (IngredientEntity ingredientEntity : ingredients) {
            this.ingredients.add(ingredientEntity);
        }
    }

    public List<IngredientEntity> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientEntity> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "RecipeEntity [description=" + description + ", id=" + id + ", ingredients=" + ingredients + ", name="
                + name + "]";
    }
}

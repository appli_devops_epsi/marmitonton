package com.example.marmitonton.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class IngredientEntity {

    @Id
    private String name;

    @ManyToMany
    @JoinTable(name = "RECIPE_INGREDIENT")
    private Set<RecipeEntity> recipes;

    public IngredientEntity() {
    }

    public IngredientEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

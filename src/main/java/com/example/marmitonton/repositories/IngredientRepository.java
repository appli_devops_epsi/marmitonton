package com.example.marmitonton.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.marmitonton.entities.IngredientEntity;

@Repository
public interface IngredientRepository extends JpaRepository<IngredientEntity, String> {
    List<IngredientEntity> findByNameLike(@Param("name") String name);
}

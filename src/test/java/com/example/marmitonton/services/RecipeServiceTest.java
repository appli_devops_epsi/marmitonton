package com.example.marmitonton.services;

import java.lang.invoke.WrongMethodTypeException;
import java.util.List;
import java.util.Optional;

import javax.el.MethodNotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.marmitonton.MarmitontonApplication;
import com.example.marmitonton.entities.RecipeEntity;
import com.example.marmitonton.repositories.RecipeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = MarmitontonApplication.class)
public class RecipeServiceTest {

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private RecipeRepository recipeRepository;

    private RecipeEntity RECETTE_DE_TEST;

    @Before
    public void setup() {
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setDescription("Recipe de configuration");
        recipeEntity.setName("Bananabread");

        this.RECETTE_DE_TEST = this.recipeRepository.save(recipeEntity);
    }

    @Test
    public void testCreateRecipeShouldBePresent() {
        // Given
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setDescription("Description");
        recipeEntity.setName("Gateau");

        // When
        recipeEntity = this.recipeService.createRecipe(recipeEntity);

        // Then
        Assert.assertTrue(this.recipeRepository.existsById(recipeEntity.getId()));
    }

    @Test(expected = WrongMethodTypeException.class)
    public void testCreateRecipeWithIdShouldThrowException() {
        // Given
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setId(1);
        recipeEntity.setDescription("Description");
        recipeEntity.setName("Gateau");

        // When
        recipeEntity = this.recipeService.createRecipe(recipeEntity);
    }

    @Test
    public void testGetRecipeShouldExists() {
        // Given
        int recipeId = RECETTE_DE_TEST.getId();

        // When
        Optional<RecipeEntity> recipeEntityRetrieved = this.recipeService.getRecipe(recipeId);

        // Then
        Assert.assertTrue(recipeEntityRetrieved.isPresent());
    }

    @Test
    public void testGetRecipeByNameContainsShouldExists() {
        // Given
        String name = "nana";

        // When
        List<RecipeEntity> recipeEntityRetrieved = this.recipeService.findByNameContains(name);

        // Then
        Assert.assertTrue(!recipeEntityRetrieved.isEmpty());
    }

    @Test
    public void testUpdateRecipe() {
        // Given
        RecipeEntity recipeEntity = this.RECETTE_DE_TEST;
        recipeEntity.setName("Test");

        // When
        this.recipeService.updateRecipe(recipeEntity);
        RecipeEntity updatedRecipe = this.recipeRepository.findById(recipeEntity.getId()).get();

        // Then
        Assert.assertEquals(recipeEntity.getName(), updatedRecipe.getName());
    }

    @Test(expected = MethodNotFoundException.class)
    public void testUpdateRecipeWithoutIdShouldThrowException() {
        // Given
        RecipeEntity recipeEntity = this.RECETTE_DE_TEST;
        recipeEntity.setName("Test");
        recipeEntity.setId(null);

        // When
        this.recipeService.updateRecipe(recipeEntity);
    }

    @Test
    public void testDeleteRecipeShouldNotExist() {
        // Given
        int recipeId = this.RECETTE_DE_TEST.getId();

        // When
        this.recipeService.deleteRecipe(recipeId);

        // Then
        Assert.assertFalse(this.recipeRepository.existsById(recipeId));
    }

}

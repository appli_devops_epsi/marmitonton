package com.example.marmitonton.services;

import java.lang.invoke.WrongMethodTypeException;
import java.util.List;
import java.util.Optional;

import javax.el.MethodNotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.marmitonton.MarmitontonApplication;
import com.example.marmitonton.entities.IngredientEntity;
import com.example.marmitonton.repositories.IngredientRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = MarmitontonApplication.class)
public class IngredientServiceTest {

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private IngredientRepository ingredientRepository;

    private IngredientEntity INGREDIENT_DE_TEST;

    @Before
    public void setup() {
        IngredientEntity ingredientEntity = new IngredientEntity();
        ingredientEntity.setName("Bananabread");

        this.INGREDIENT_DE_TEST = this.ingredientRepository.save(ingredientEntity);
    }

    @Test
    public void testCreateIngredientShouldBePresent() {
        // Given
        IngredientEntity ingredientEntity = new IngredientEntity();
        ingredientEntity.setName("Gateau");

        // When
        ingredientEntity = this.ingredientService.createIngredient(ingredientEntity);

        // Then
        Assert.assertTrue(this.ingredientRepository.existsById(ingredientEntity.getName()));
    }

    @Test
    public void testGetIngredientShouldExists() {
        // Given
        String ingredientName = INGREDIENT_DE_TEST.getName();

        // When
        Optional<IngredientEntity> ingredientEntityRetrieved = this.ingredientService.getIngredient(ingredientName);

        // Then
        Assert.assertTrue(ingredientEntityRetrieved.isPresent());
    }

    @Test
    public void testGetIngredientByNameLikeShouldExists() {
        // Given
        String name = INGREDIENT_DE_TEST.getName();

        // When
        List<IngredientEntity> ingredientEntityRetrieved = this.ingredientService.getIngredientByNameLike(name);

        // Then
        Assert.assertTrue(!ingredientEntityRetrieved.isEmpty());
    }

    @Test
    public void testUpdateIngredient() {
        // Given
        IngredientEntity ingredientEntity = this.INGREDIENT_DE_TEST;
        ingredientEntity.setName("Test");

        // When
        this.ingredientService.updateIngredient(ingredientEntity);
        IngredientEntity updatedIngredient = this.ingredientRepository.findById(ingredientEntity.getName()).get();

        // Then
        Assert.assertEquals(ingredientEntity.getName(), updatedIngredient.getName());
    }

    @Test(expected = MethodNotFoundException.class)
    public void testUpdateIngredientWithoutIdShouldThrowException() {
        // Given
        IngredientEntity ingredientEntity = this.INGREDIENT_DE_TEST;
        ingredientEntity.setName(null);

        // When
        this.ingredientService.updateIngredient(ingredientEntity);
    }

    @Test
    public void testDeleteIngredientShouldNotExist() {
        // Given
        String ingredientName = this.INGREDIENT_DE_TEST.getName();

        // When
        this.ingredientService.deleteIngredient(ingredientName);

        // Then
        Assert.assertFalse(this.ingredientRepository.existsById(ingredientName));
    }

}
